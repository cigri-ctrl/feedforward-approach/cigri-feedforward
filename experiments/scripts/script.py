from nixos_compose.nxc_execo import get_oar_job_nodes_nxc, build_nxc_execo

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os
import shutil
import json
import yaml
import time


class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--nxc_build_file', help='Path to the NXC deploy file')
        parser.add_argument('--walltime', help='walltime in hours')
        parser.add_argument('--result_dir', help='where to store results')
        parser.add_argument('--outfile', help='outfile')
        parser.add_argument('config_file', help='config file')
        self.nodes = {}
        self.oar_job_id = -1
        self.nb_nodes = 3
        self.flavour = "g5k-image"

    def init(self):
        assert(self.args.config_file)
        walltime_hours = float(self.args.walltime) if self.args.walltime else 2
        nxc_build_file = self.args.nxc_build_file

        site = "grenoble"
        cluster = "dahu"

        oar_job = reserve_nodes(self.nb_nodes, site, cluster, "deploy",  walltime=walltime_hours*60*60)
        self.oar_job_id, site = oar_job[0]

        roles_quantities = {"oar-server": 1, "node": 1, "cigri": 1}

        self.nodes = get_oar_job_nodes_nxc(
            self.oar_job_id,
            site,
            flavour_name=self.flavour,
            compose_info_file=nxc_build_file,
            roles_quantities=roles_quantities)
        print(self.nodes)

    def run(self):
        result_dir = self.args.result_dir if self.args.result_dir else os.getcwd()

        logger.info("[SETUP] Create result folder")
        zip_archive_name = f"{result_dir}/results_cigri_{self.oar_job_id}"
        outfile = self.args.outfile[:-4] if self.args.outfile else zip_archive_name

        folder_name = f"{result_dir}/result_cigri_{self.oar_job_id}"
        create_folder(folder_name)
        
        logger.info("[SETUP] Config file detected. Loading the config")
        config_file = self.args.config_file
        with open(config_file, 'r') as yaml_file:
            config = yaml.safe_load(yaml_file)

            nb_jobs = config["nb_jobs"]
            sleep_time = config["sleep_time"]
            config_ctrlr = config["ctrlr"]

            logger.info("[SETUP] Coping the config file to the result folder")
            with open(f"{folder_name}/expe.yaml", "w") as expe_file:
                yaml.dump(config, expe_file)

            logger.info("[SETUP] Dumping the controller config file to the result folder")
            with open(f"{folder_name}/config.json", "w") as config_ctrlr_file:
                json.dump(config_ctrlr, config_ctrlr_file)


        logger.info("[SETUP] Generate CiGri Campaign")
        run_gen_campaign = Remote(f"sudo -u user1 gen_campaign {nb_jobs} {sleep_time} 0", self.nodes["cigri"][0], connection_params={'user': 'root'})
        run_gen_campaign.run()
        

        logger.info("[SETUP] Submit CiGri Campaign")
        run_sub_campaign = Remote(f"sudo -u user1 gridsub -f /home/user1/campaign_{nb_jobs}j_{sleep_time}s_0M.json", self.nodes["cigri"][0], connection_params={'user': 'root'})
        run_sub_campaign.run()


        logger.info("[SETUP] Copying the controller config to /tmp/config.json")
        Remote(f"cp {folder_name}/config.json /tmp/config.json", self.nodes["cigri"][0], connection_params={'user': 'root'}).run()

        
        gridstat = GridstatInfo(folder_name, self.nodes["cigri"][0])
        
        while True:
            time.sleep(30)
            gridstat.update_gridstat_data()
            progress = gridstat.get_progress()
            logger.info(f"[RUNNING] {progress}% jobs completed")
            if gridstat.has_event():
                logger.warning("[RUNNING] A CiGri campaign has some event!!!")
                break
            if progress is None or progress == 100:
                logger.info("[RUNNING] All CiGri jobs are complete")
                break
        
        logger.info("[CLEANING] Get the CiGri logs")
        Remote(f"cp /tmp/cigri.log {folder_name}/cigri.log",
               self.nodes["cigri"][0],
               connection_params={'user': 'root'}).run()
        
        logger.info("[CLEANING] Get the controller log")
        Remote(f"cp /tmp/log.txt {folder_name}/ctrlr.csv",
               self.nodes["cigri"][0],
               connection_params={'user': 'root'}).run()
        
        logger.info("[CLEANING] Get the OAR DB dump")
        Remote(f"dump_oar > {folder_name}/oar_db.csv",
               self.nodes["oar-server"][0],
               connection_params={'user': 'root'}).run()

        logger.info(f"[CLEANING] Zipping the data -> {outfile}.zip")
        zip_files(outfile, folder_name)
        logger.info(f"[CLEANING] Removing the folder {folder_name}")
        remove_folder(folder_name)

        logger.info(f"[CLEANING] Giving back the resources")
        oardel([(self.oar_job_id, "grenoble")])

def reserve_nodes(nb_nodes, site, cluster, job_type, walltime=3600):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=[job_type]), site)])
    return jobs

def zip_files(name, folder):
    """
    zip the folder
    """
    shutil.make_archive(name, "zip", folder)

def create_folder(folder):
    """
    Create a folder
    """
    if not os.path.exists(folder):
        os.makedirs(folder)

def remove_folder(folder):
    """
    Remove the folder
    """
    shutil.rmtree(folder)


class GridstatInfo:

    def __init__(self, folder, cigri_node):
        self.folder = folder
        self.path = f"{self.folder}/gridstat.json"
        self.cigri_node = cigri_node
        
        self.gridstat_data = None

    def update_gridstat_data(self):
        Remote(f"gridstat -d > {self.path}",
               self.cigri_node,
               connection_params={'user': 'root'}).run()
        with open(self.path, "r") as gridstat_json:
            self.gridstat_data = json.load(gridstat_json)

    def get_number_of_running_campaigns(self):
        if self.gridstat_data:
            return int(self.gridstat_data["total"])
        return -1

    def has_event(self):
        if self.gridstat_data:
            return any(map(lambda x: x["has_events"], self.gridstat_data["items"]))
        return False

    def get_progress(self):
        if self.gridstat_data:
            nb_finished_jobs = sum(map(lambda x: x["finished_jobs"], self.gridstat_data["items"]))
            nb_total_jobs = sum(map(lambda x: x["total_jobs"], self.gridstat_data["items"]))
            return (nb_finished_jobs * 100 / nb_total_jobs) if nb_total_jobs > 0 else None
        return None


if __name__ == "__main__":
    ENGINE = MyEngine()
    try:
        ENGINE.start()
    except Exception as ex:
        print(f"Failing with error {ex}")
