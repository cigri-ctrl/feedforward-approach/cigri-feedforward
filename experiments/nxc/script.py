from nixos_compose.nxc_execo import get_oar_job_nodes_nxc

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes, wait_oar_job_start
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os
import shutil
import sched
import csv
import time
import json
import datetime
import re
import math

def get_profiles(json_data):
    return json_data["profiles"] 
    
def get_jobs(json_data):
    return json_data["jobs"]
        

class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--nxc_build_file', help='Path to the NXC deploy file')
        parser.add_argument('--walltime', help='walltime in hours')
        parser.add_argument('--workload', help='workload')
        parser.add_argument('--output', help="where the results will be stored")
        parser.add_argument('--port', help="port for the http server")

        parser.add_argument('--cigri_nb_jobs', help="number of cigri jobs in campaign")
        parser.add_argument('--cigri_exec_time', help="duration of the cigri jobs in seconds")

        parser.add_argument('--controller_type', help="controller type")
        parser.add_argument('--ks', help="ks")
        parser.add_argument('--mp', help="mp")
        parser.add_argument('--horizon', help="horizon")

        self.nodes = {}
        self.oar_job_id = -1
        self.nb_nodes = -1
        self.flavour = "g5k-image"
        self.workload = None
        self.output = None

    def init(self):
        self.nb_nodes = 3

        walltime_hours = float(self.args.walltime) if self.args.walltime else 4
        nxc_build_file = self.args.nxc_build_file
        # self.flavour = self.args.flavour if self.args.flavour else "g5k-image"
        # self.csv_file = self.args.csv_file if self.args.csv_file else "results_batsim.csv"
        self.workload = self.args.workload
        self.output = self.args.output
        port = int(self.args.port) if self.args.port else 0

        site = "grenoble"
        cluster = "dahu"

        oar_job = reserve_nodes(self.nb_nodes, site, cluster, "deploy" if self.flavour == "g5k-image" else "allow_classic_ssh",  walltime=walltime_hours*60*60)
        self.oar_job_id, site = oar_job[0]

        roles_quantities = {"oar-server": ["oar-server"], "cigri": ["cigri"], "node": ["node1"]}

        wait_oar_job_start(self.oar_job_id, site)
        self.nodes = get_oar_job_nodes_nxc(
            self.oar_job_id,
            site,
            flavour_name=self.flavour,
            compose_info_file=nxc_build_file,
            roles_quantities=roles_quantities,
            port=port)
        print(self.nodes)

    def oar_command(self, job, profiles, writer, debug=False):
        nb_nodes = job["res"]
        walltime = str(datetime.timedelta(seconds=job["walltime"]))
        runtime = profiles[job["profile"]]["runtime"]
        logger.info(f"Submitting HP job of {nb_nodes} resources and runtime of {runtime}")
        oar_cmd = f"sudo -u user2 oarsub -l resource_id={nb_nodes},walltime={walltime} \"sleep {runtime}\""
        subjob = Remote(oar_cmd, self.nodes["oar-server"], connection_params={'users': 'root'})
        process = subjob.run()
        process_oar = process.processes[0].stdout

        stdout = process_oar
        find_job = re.search("\\nOAR_JOB_ID=(.*)\\n", stdout)
        if find_job:
            job_id = int(find_job.group(1))
            writer.writerow({"batsim_id": job["id"], "oar_id": job_id})
        else:
            print("JOB_NOT_SUBMITTED_BATSIM_JOB_ID: {}".format(job["id"]))

    def get_gridstat_data(self, path):
        """
        Returns the number of campaigns CiGri that are terminated
        """
        Remote(f"gridstat -d > {path}",
               self.nodes["cigri"],
               connection_params={'user': 'root'}).run()
        with open(path, "r") as gridstat_json:
            gridstat_data = json.load(gridstat_json)
        return gridstat_data

    def run(self):
        folder_name = self.output[:-4]
        logger.info(f"Creating folder {folder_name}")
        create_folder(folder_name)

        oarsub_sched = sched.scheduler(time.time, time.sleep)
        csv_file = open(f"{folder_name}/batsim_oar.csv", "w")
        writer = csv.DictWriter(csv_file, fieldnames=["batsim_id", "oar_id"])
        writer.writeheader()

        with open(self.workload, "r") as workload_file:
            json_data = json.load(workload_file)

            logger.info(f"Saving the workload in {folder_name}/workload.json")
            with open(f"{folder_name}/workload.json", 'w') as json_file:
                json.dump(json_data, json_file)

            jobs = get_jobs(json_data)
            profiles = get_profiles(json_data)
            do_oarsub = lambda j: self.oar_command(j, profiles, writer)
            logger.info("Submitting HP jobs to the python scheduler")
            for job in get_jobs(json_data):
                oarsub_sched.enter(job["subtime"], 0, do_oarsub, kwargs={'j': job})

        # Rights on the NFS
        logger.info("Give rights to on the NFS server")
        Remote("chmod 777 /srv", self.nodes["oar-server"], connection_params={'users':'root'}).run()


        # Generate Campaign
        nb_jobs = int(self.args.cigri_nb_jobs)
        sleep_time = int(self.args.cigri_exec_time)
        logger.info("Generating Campaign for user1")
        gen_campaign = Remote(f"sudo -u user1 gen_campaign {nb_jobs} {sleep_time} 0", self.nodes["cigri"], connection_params={'users': 'root'})
        gen_campaign.run()

        campaign_save = f"{folder_name}/campaign.json"
        logger.info(f"Saving the campaign in {campaign_save}")
        Remote(f"cp /home/user1/campaign_{nb_jobs}j_{sleep_time}s_0M.json {campaign_save}", self.nodes["cigri"], connection_params={'users': 'root'}).run()

        # Submit Campaign
        logger.info("Submitting the Campaign to CiGri")
        sub_campaign = Remote(f"sudo -u user1 gridsub -f /home/user1/campaign_{nb_jobs}j_{sleep_time}s_0M.json", self.nodes["cigri"], connection_params={'users':'root'})
        sub_campaign.run()

        # Create controller
        logger.info("Starting the Contoller")
        # create_ctrlr = Remote("echo '{\"type\":\"step\",\"u_values\":[10]}' > /tmp/config.json", self.nodes["cigri"], connection_params={'users':'root'})
        alpha = 30.0 / sleep_time
        rmax = 32
        # computation of the controller gains
        a = 1
        b = 1
        ks = float(self.args.ks)
        mp = float(self.args.mp)
        r = math.exp(-4/ks)
        def log(x):
            if x == 0:
                return -math.inf
            return math.log(x)
        theta = math.pi * log(r) / log(mp)
        
        kp = (a - r * r) / b
        ki = (1 - 2 * r * math.cos(theta) + r * r) / b
        
        horizon = int(self.args.horizon) * 30
        
        controller_config = {
            "type": self.args.controller_type,
            "alpha": alpha,
            "rmax": rmax,
            "kp": kp,
            "ki": ki,
            "ks": ks,
            "mp": mp,
            "ref": rmax,
            "horizon_now": horizon,
            "horizon_then": horizon
        }

        with open(f"{folder_name}/info.csv", "w") as csv_file_info:
            writer_info = csv.DictWriter(csv_file_info, fieldnames=["controller", "horizon", "ks", "mp", "cigri_nb_jobs", "cigri_sleep_time", "workload"])
            writer_info.writeheader()
            writer_info.writerow({"controller": self.args.controller_type, "horizon": horizon, "ks": ks, "mp": mp, "cigri_nb_jobs": nb_jobs, "cigri_sleep_time": sleep_time, "workload": self.workload})



        create_ctrlr = Remote(f"echo '{json.dumps(controller_config)}' > /tmp/config.json", self.nodes["cigri"], connection_params={'users':'root'})
        create_ctrlr.run()

            
        logger.info("Starting the replay of the workload")
        oarsub_sched.run()
        csv_file.close()

        while True:
            gridstat_data = self.get_gridstat_data(f"{folder_name}/gridstat.json")
            progress = get_progress(gridstat_data)
            logger.info(f"Progress {progress} %")
            if progress is not None and progress < 100:
                time.sleep(30)
            else:
                break

        # Get CiGri Data
        logger.info("Get the data from CiGri")
        Remote(f"cp /tmp/log.txt {folder_name}/cigri.csv", self.nodes["cigri"], connection_params={'users':'root'}).run()

        # Get oar db
        output_oar = f"{folder_name}/oar.csv"
        logger.info("Saving OAR DB")
        oar_db = Remote(f"dump_oar > {output_oar}", self.nodes["oar-server"], connection_params={'users':'root'})
        oar_db.run()

        # Zip the folder
        logger.info(f"Zipping the data -> {folder_name}.zip")
        zip_files(folder_name, folder_name)

        # Removing the folder
        logger.info(f"Removing folder {folder_name}")
        remove_folder(folder_name)
        
        logger.info("Giving back the resources")
        oardel([(self.oar_job_id, "grenoble")])


def get_number_of_running_campaigns(gridstat_data):
    """
    Returns the number of running CiGri campaigns
    """
    return gridstat_data["total"]

def has_events(gridstat_data):
    """
    Return either there are some events in a CiGri campaign
    """
    return any(map(lambda x: x["has_events"], gridstat_data["items"]))

def get_progress(gridstat_data):
    """
    Returns the percentage of jobs executed
    """
    try:
        return sum(map(lambda x: x["finished_jobs"], gridstat_data["items"])) * 100 / sum(map(lambda x: x["total_jobs"], gridstat_data["items"]))
    except:
        return None


def zip_files(name, folder):
    """
    zip the folder
    """
    shutil.make_archive(name, "zip", folder)

def create_folder(folder):
    """
    Create a folder
    """
    if not os.path.exists(folder):
        os.makedirs(folder)

def remove_folder(folder):
    """
    Remove the folder
    """
    shutil.rmtree(folder)



def reserve_nodes(nb_nodes, site, cluster, job_type, walltime=3600):
    # jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=[job_type, "day"]), site)])
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=[job_type, "night"]), site)])
    # jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=[job_type, "inner=2282176"]), site)])
    return jobs



if __name__ == "__main__":
    ENGINE = MyEngine()
    try:
        ENGINE.start()
    except Exception as ex:
        print(f"Failing with error {ex}")
