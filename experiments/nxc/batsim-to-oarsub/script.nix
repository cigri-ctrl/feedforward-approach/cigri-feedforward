{ pkgs }:

{
  batsim-to-oarsub = pkgs.writeScriptBin "batsim-to-oarsub" ''
    ${pkgs.python3}/bin/python3 ${./batsim_to_oarsub.py} $@
  '';
}
