{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/22.05";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };

    rPkgs = with pkgs.rPackages; [
      tidyverse
      geometry
    ];

    myR = pkgs.rWrapper.override { packages = rPkgs; };

    rmdPkgs = with pkgs.rPackages; [
        rmarkdown
        markdown
        knitr
        tinytex
        magick
        codetools
      ];

      myRmd = pkgs.rWrapper.override { packages = rPkgs ++ rmdPkgs; };
      myRstudio = pkgs.rstudioWrapper.override { packages = rPkgs; };
  in
  {
    devShells.${system} = {
      rshell = pkgs.mkShell {
        buildInputs = [ myR ];
      };

      rstudio = pkgs.mkShell {
        buildInputs = [ myRstudio ];
      };

    };
  };
}
