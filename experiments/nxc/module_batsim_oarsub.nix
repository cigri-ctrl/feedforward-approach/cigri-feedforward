batsim-oarsub:
{ pkgs, ...}:
{
  imports = [
    batsim-oarsub.nixosModules.batsim_to_oarsub
  ];
}
