{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
    qornflakes.url = "github:GuilloteauQ/qornflakes";
  };

  outputs = { self, nixpkgs, qornflakes }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
    qorn = qornflakes.packages.${system};

    rPkgs = with pkgs.rPackages; [
      tidyverse
      lubridate
      reshape2
      viridis
      zoo
      corrr
      e1071
      EnvStats
      goftest
      fitdistrplus
      evd
      ggExtra
      patchwork
      qorn.geomtextpath
    ];

    myR = pkgs.rWrapper.override { packages = rPkgs; };
    myRstudio = pkgs.rstudioWrapper.override { packages = rPkgs; };

  in
  {
    packages.${system} = {
      docker = pkgs.dockerTools.buildImage {
        name = "rdocker";
        tag = "latest";
        contents = [ myR pkgs.bashInteractive ];
        # config.entrypoint = [ "${myR}/bin/Rscript" ];
      };
    };
    devShells.${system} = {
      default = pkgs.mkShell {
        buildInputs = [ pkgs.graphviz pkgs.snakemake ];
      };

      rshell = pkgs.mkShell {
        buildInputs = [ myR ];
      };
    };
  };
}
