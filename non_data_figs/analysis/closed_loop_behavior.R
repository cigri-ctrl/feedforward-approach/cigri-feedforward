library(tidyverse)

args = commandArgs(trailingOnly=TRUE)
outname = args[1]

# alpha <- coef(model_load)[1]
# beta1 <- coef(model_load)[2]
beta2 <- 0.017 # coef(model_load)[3]
# gamma <- coef(model_load)[4]
dt_cigri <- 30
a <- (exp(-5 / 60)) ** (dt_cigri / 5)
max_duration <- 20 # iterations
start_step <- 100

reference_value <- 4.0
filesize <- 100
b <- 0.017 + 0.000643 * filesize 
# (as.numeric(beta2) + as.numeric(gamma) * filesize) * (1 - a)
input <- seq(max_duration)

ks_values <- c(5, 10, 15)
Mp_values <- c(0.0, 0.25, 0.5)

gains <- tidyr::crossing(ks = ks_values, Mp = Mp_values)

gains <- gains %>% mutate(r = exp(-4 / ks)) %>% mutate(theta = pi * log(r) / log(Mp)) %>% mutate(kp = (a - r * r) / b) %>% mutate(ki = (1 - 2 * r * cos(theta) + r * r) / b)

get_output <- function(kp, ki) {
  input <- seq(max_duration)
  output <- seq(max_duration)
  time <- seq(max_duration)
  output[1] <- 0
  cumul_error <- 0
  for (i in 1:(max_duration - 1)) {
      error <- reference_value - output[i]
      cumul_error <- cumul_error + error
      input[i] <- kp * error + ki * cumul_error
      output[i + 1] <- a * output[i] + b * input[[i]]
  }
  tibble(time = time, output = output)
}

gains <- gains %>% group_split(ks, Mp, kp, ki) %>% map_df(~crossing(.x, data = get_output(.x$kp, .x$ki))) %>% mutate(Mp = factor(Mp)) %>% mutate(ks = factor(ks))

ggplot(gains, aes(x = data$time, y = data$output)) +
  facet_grid(ks ~ Mp, labeller = label_both) +
  geom_step() +
  geom_hline(yintercept = reference_value, linetype = "dashed", color = "red", size = 0.4) +
  geom_segment(
    data = gains %>% filter(ks == 10 & Mp == 0.25),
    x = 1, y = 7,
    xend = 10, yend = 7,
    color = "orange",
    arrow = arrow(length = unit(0.07, "npc"))) +
  geom_text(
    data = gains %>% filter(ks == 10 & Mp == 0.25),
    x = 5, y = 8, label = "ks", color = "orange"
  ) +
  geom_segment(
    data = gains %>% filter(ks == 10 & Mp == 0.25),
    x = 2.5, y = 4,
    xend = 2.5, yend = 6,
    color = "orange",
    arrow = arrow(length = unit(0.07, "npc"))) +
  geom_text(
    data = gains %>% filter(ks == 10 & Mp == 0.25),
    x = 1.1, y = 5, label = "Mp", color = "orange"
  ) +
  theme_bw() +
  xlab("Iterations") +
  ylab("Output") +
  # scale_y_continuous(breaks = seq(0, 10, 2)) +
  scale_y_continuous(breaks = c()) +
  theme(strip.background = element_blank()) +
  # theme(axis.text=element_text(size=12),
        # strip.text.x = element_text(size = 15),
        # strip.text.y = element_text(size = 15),
        # axis.title=element_text(size=17,face="bold")) +
  ggtitle("System closed-loop behaviour for different values of (ks, Mp)")

ggsave(outname, width = 5.2, height = 4)
