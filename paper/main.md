---
title: Reducing Wasted Cluster Computing Power with \CT
author:
  - name: "Quentin Guilloteau, Eric Rutten, Olivier Richard"
    affiliation: Univ. Grenoble Alpes, Inria, CNRS, LIG
    location: 38000 Grenoble France
    email: Firstname.Lastname@inria.fr
  # - name: "Anonymous Author(s)"
  #   affiliation: Anonymous Affiliation(s)
  #   location: Anonymous Location(s)
  #   email: Anonymous Email Address(es)
keywords:
  - Grid Computing
  - Idle Resources
  - Control Theory
  - Feedback Computing
  - Autonomic Computing
  
numbersections: yes
fontsize: 10pt
papersize: a4paper
classoption: conference
lang: en
bibliography: references.bib
abstract: |
    TODO
header-includes: |
  \usepackage{booktabs}
  \usepackage{longtable}
  \usepackage{multirow}
  \usepackage{hyperref}
  \usepackage{xcolor}
  \usepackage{listings}
  \usepackage{algorithm2e}
  \usepackage{subcaption}
  \usepackage[inline]{enumitem}
  \newcommand{\CT}{Control~Theory}
  \newcommand{\io}{\emph{I/O}}
  \newcommand{\qos}{\emph{Quality-of-Service}}
  \newcommand{\repro}{reproducibility}
  \newcommand{\Repro}{Reproducibility}
  \newcommand{\ie}{\emph{i.e.,}}
  \newcommand{\eg}{\emph{e.g.,}}
  \newcommand{\nix}{\emph{Nix}}
  \newcommand{\nixos}{\emph{NixOS}}
  \newcommand{\nxc}{\emph{NixOS Compose}}
  \newcommand{\enos}{\emph{EnOSlib}}
  \newcommand{\grid}{\emph{Grid'5000}}
  \newcommand{\kam}{\emph{Kameleon}}
  \newcommand{\kad}{\emph{Kadeploy}}
  \newcommand{\mel}{\emph{Melissa}}
  \newcommand{\cigri}{\emph{CiGri}}
  \newcommand{\oar}{\emph{OAR}}
  \newcommand{\bag}{\emph{Bag-of-Tasks}}
  \newcommand{\be}{\emph{Best-Effort}}
  \newcommand{\gricad}{\emph{Gricad}}
  \newcommand\todo[1]{{\textcolor{red}{TODO: #1}}}
  \definecolor{commentcolour}{rgb}{0.04,0.43,0.17}
  \definecolor{keywordcolour}{rgb}{0.65,0.15,0.64}
  \definecolor{backcolour}{rgb}{1,1,1}
  \definecolor{linenumbercolour}{rgb}{0.1,0.1,0.1}
  \definecolor{stringcolour}{rgb}{0.56,0.06,0.49}
  \lstdefinestyle{mystyle}{
      backgroundcolor=\color{backcolour},   
      commentstyle=\color{commentcolour},
      keywordstyle=\color{keywordcolour},
      numberstyle=\tiny\color{linenumbercolour},
      stringstyle=\color{stringcolour},
      basicstyle=\ttfamily\footnotesize,
      breakatwhitespace=false,         
      breaklines=true,                 
      captionpos=b,                    
      keepspaces=true,                 
      numbers=left,                    
      numbersep=5pt,                  
      showspaces=false,                
      showstringspaces=false,
      showtabs=false,                  
      tabsize=2,
      framexrightmargin=-12pt
  }
  \lstset{style=mystyle, frame=single}
---




