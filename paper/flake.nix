{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/21.11";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in
  {
    packages.${system} = rec {
      paper = pkgs.stdenv.mkDerivation {
        name = "paper";
        src = ./.;
        buildInputs = with pkgs; [
          pandoc
          texlive.combined.scheme-full
          ninja
          bibtool
          rubber
        ];
        buildPhase = ''
          mkdir -p $out
          ninja
          cp main.pdf $out
        '';
        installPhase = ''
          echo "Skipping installPhase"
        '';
      };
      default = paper;
    };
    devShells.${system} = {
      default = pkgs.mkShell {
        buildInputs = with pkgs; [
          pandoc
          texlive.combined.scheme-full
          ninja
          vale
          bibtool
          rubber
          entr
        ];
        shellHook = ''
          ls main.md sections/*.md | entr -s "ninja"
        '';
      };
    };
  };
}
