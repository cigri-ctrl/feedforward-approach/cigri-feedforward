# \CT\ Methodology

This Section presents a \CT\ workflow to design a controller for a software system \cite{filieri2017control, filieri2015software}.

## Problem definition {#sec:methodo:pb_def}

The first step is to define the objectives, characteristics and range of action of the problem.

In this work, we want to improve the usage of a set of computing clusters.

## Control formulation

Then, we need to define the means of action and sensors at our disposal to act and read the system.

Our knob of action is the number of \bag\ jobs submitted by \cigri\ at each of its iterations.
To sense the current state of the clusters, we can query the API of the \oar\ schedulers to get the number of resources currently used, the number of \be\ jobs waiting, or the provisional schedule of the jobs.

## System Analysis {#sec:methodo:sys_anal}

Once the knobs and sensors defined, their relation need to be analyzed.
Meaning how the knobs influence the values of the sensors.
This analysis requires a specific experiments phase called *identification*.
This phase might highlight some new characteristics or challenges of the problem, which will require to update the problem definition (Section \ref{sec:methodo:pb_def})

We present this phase in Section \todo{}.

## Model and control design

With the system analysis (Section \ref{sec:methodo:sys_anal}) and the problem definition (Section \ref{sec:methodo:pb_def}), we can choose and define an adequate controller.
There exists several types of controllers with each their pros and cons.
Once chosen, the controller needs to be tune to the system.
This tuning is usually done with \CT\ theorems or toolboxes.  

We present this phase in Section \todo{}.

## Evaluation

Finally, we can evaluate the performance of the closed-loop system with the controller.

We present this phase in Section \todo{}.



