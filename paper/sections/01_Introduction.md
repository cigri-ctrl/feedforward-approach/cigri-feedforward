# Introduction

<!-- comment
- scientists need to perform computations
- computations need to be more and more accurate
- too large to run on laptop
- run on cluster, shared machines
- go through a reservation mechanism
- but sometimes not enough demand to use all the resources
- what to do with the idle resources?
-->


Scientists from most fields have to run computations such as simulations (\eg\ weather forecast, molecule folding), data analysis, or others.
These computations require high precision and accuracy that demand a computing power order of magnitude higher than a laptop.
Thus, scientists turn to more powerful machines linked together to form a *supercomputer*.
Supercomputers are very expensive, and are therefore shared between many users from different research groups and laboratories.
A reservation process is used to access machines and execute computations grouped as *jobs*.
Users submit their jobs, along with the estimate duration and the required number of machines to the *scheduler*, which is responsible for mapping the jobs to the physical machines.
However, a lack of demand, among others reasons, can lead to idling among machines, representing a non-negligible loss of computing power.


<!-- comment
- researchers have been looking at this problem
- small and interruptible jobs
- but still being executed and impact the shared resources
- another sources of loss of computing power: killed jobs
- no checkpointing, so computation lost and need to be restarted

- in this article....
-->


Researchers have been studying the problem of harvesting idle resources [And04; Mer+17; Fre+01] by submitting jobs that are more flexible (smaller, interruptible).
These jobs are viewed as second class citizens by the scheduler and can be killed if needed.
However, these jobs are still being executed on the cluster and impact the shared resources of the cluster (\eg\ file-system, communication network), thus inevitably disturbing the jobs of premium users.
Meaning, there is a trade-off to exploit between the amount of harvesting and the maximum perturbation that these premium users can accept.


Unfortunately, these solutions introduce a new source of computing power waste by killing jobs.
Indeed, most of these small jobs do not implement a check pointing mechanism, thus, all the computation done before the jobs are killed will be lost and will need to be started again.



In this article, we tackle the problem of harvesting the idle resources of a cluster while minimizing the total amount of wasted computing time (either idle or killed). 


<!-- comment
- based our work on \cigri
- use the autonomic computing field to regulate the injection of best-effort jobs
- use tools from control theory as they have been proven on numerous physical system
- inertia
- implement a feedforward to anticipate the killing
-->


To reach this goal, we based our work on the existing \cigri\ middleware.
\cigri\ submits jobs from \bag\ applications to use the idle resources of a set of cluster.
The limitation of \cigri\ is the lack of information from the current state of the cluster for the decision on number of jobs to submit.
This blind decision can lead to, both, an underutilization of the resources or an overload of shared resources.
To understand the state of the cluster and improve the \cigri\ decision-making, we need sensors on the system.
This is the idea of the Autonomic Computing field.
The implementation of the autonomic controller (AC) can be done with several technics (\eg\ "bang-bang", IA).
The intrinsic inertia of our system led us to consider tools from the Control Theory to implement the AC in our system.
Control Theory has been proven on numerous physical systems, and more recently on some computing systems.


The paper is structured as follows.
Section \ref{sec:sota} gives the state-of-the-art on tools related to the reproducible deployment of distributed systems, and positions \nxc in it.
Section \ref{sec:pres_nxc} presents \nxc, its main concepts, the external concepts it relies on, and the users' utilization workflow for setting up a complete reproducible system and software stack.
Section \ref{sec:how_it_works} gives technical details on how \nxc\ works.
Section \ref{sec:melissa} presents how \nxc\ can be used on a complex example that combines several distributed middlewares.
Section \ref{sec:eval} offers experimental performance results of \nxc\ against standard state-of-the-art tools using multiple metrics.
Finally, Section \ref{sec:conclu} concludes the paper with final remarks and perspectives on reproducible works and environments.
