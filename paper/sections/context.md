# Context {#sec:context}

## The \gricad\ Computing Center

The \gricad\ computing center\footnote{\url{https://gricad.univ-grenoble-alpes.fr}} provides computing and storing infrastructures to the researchers of the region of Grenoble, France.
The center is composed of several computing clusters, each with a focus (\eg\ `luke` for data processing, `froggy` for HPC, or `dahu` for HPCDA\footnote{\emph{High Performance Computing and Data Analysis}}).
These clusters are linked together to from a computing grid.
This grid is also impacted by the issue of unused resources. 

## The \cigri\ middleware

\cigri\ is a computing grid middleware set up in the \gricad\ computing center.
It interacts with the schedulers \oar\ \cite{capit_batch_2005} of each cluster.
The goal of \cigri\ is to use the idle resources of the entire computing grid.

Users of \cigri\ submit \bag\ applications to the middleware.
These applications are composed of numerous small and independent tasks making them a perfect candidate for the harvesting the idle resources.
An example of such application is Monte-Carlo simulations, where the user will execute a large batch of random experiments to conclude on the aggregated results.
 
Once the application submitted to the middleware, \cigri\ will submit batches of jobs to the clusters of the grid.
The jobs are submitted to the schedulers with the lowest priority (\be), which allows \oar\ to kill the \be\ job is a premium user needs the resources.
 
\todo{a new \cigri\ picture here}


## \cigri\ jobs

The hierarchy of \cigri\ jobs is as follows:

- a *job* belongs to a *campaign*

- a *campaign* belongs to a *project*

From the point of view of \cigri, a *campaign* is a \bag\ application.

One example of project is the processing of massive GPS data for the deformation of the Earth surface.
There are several geographic stations, with a lot of GPS data.
The processing of each station can be grouped as a *campaign*, where the processing of a subregion is a *job*. 

In this section, we present some characteristics of the \cigri\ jobs.
The data presented in this section represent the jobs submitted from January $2^{nd}$ 2017 to October $8^{th}$ 2021 (58 months), representing about 30 millions jobs. 
More details can be found in \cite{guilloteau2022etude}.

### Global Characteristics

\begin{figure}
  \centering
  \includegraphics[width = 0.45\textwidth]{./figs/ecdf_comparison.pdf}
  \caption{Estimated Cumulative Distribution Function (ECDF) for the execution times of the \bag\ jobs from the grids \gricad\ and \emph{DAS2}.}
  \label{fig:ecdf_grid}
\end{figure}

Figure \ref{fig:ecdf_grid} compares the estimated cumulative distribution (ECDF) of the execution times of the \bag\ jobs from the grids \gricad\ and *DAS2*.
*DAS2* (Distributed ASCI Supercomputer 2) is the second iteration of Dutch universities computing grid.
We observe that the majority of the jobs have a low execution time ($< 60s$), and that about 75\% of the jobs have an execution time of the order of the minute.

### Distribution of the execution times per project

\begin{figure*}
  \centering
  \includegraphics[width = 0.9\textwidth]{./figs/histograms_project.pdf}
  \caption{Histograms of the execution times of the \bag\ jobs from the 10 projects with the most jobs in the \gricad\ grid.}
  \label{fig:histo_bag}
\end{figure*}

Figure \ref{fig:histo_bag} shows the distribution of execution times of the jobs from several projects.
We can see that for each project, there is one clear mode.
The queue of the distributions can have several explanations.
They can come from the additional non-constant time to allocate and de-allocate the resources to and from the jobs.
Or they can also be due to the performances of the distributed file system of the cluster at the time of the execution.

### Job and Campaign model used in this paper

\todo{here?}

For the remaining of this paper we will represent a campaign as a set of jobs having the exact same execution time.
In practice, we implement a job using the `sleep` command.
Choosing this job representation, instead of executing real \bag\ applications, allows us to better control the characteristics of the campaign and create all sorts of scenarios.



