## The \texttt{docker} Flavour

\todo{one sentence to summarizes}
Generates a \texttt{docker-compose} configuration.

### Construction

The \texttt{docker} flavours bases itself on the \texttt{docker-compose} application.
\nxc\ thus generates the adequate docker images for the different roles as well as the docker-compose configuration file.

\nix\ can produce docker images.
To emulate the behaviour of a real machine, \nxc\ build the \texttt{init} phase of the systems described in the composition.
This phase is stored in the \store\ and can then be called as the container command.

As \nix\ uses sets as the main data structure, we build the content of the \texttt{docker-compose} configuration file as a set and cast it as a JSON file.
We open the desired ports, mount the volumes, etc.

### Deployment

Once the \texttt{docker-compose} configuration has been built by \nxc, the latter calls the \texttt{docker-compose} binary with the adequate parameters.
To stay reproducible even during the deployment phase, the version of \texttt{docker-compose} is locked by \nxc\ with \nix.

To connect to a specific container, \nxc\ starts a \texttt{bash} instance in the target container
This means that for the construction of the container image \nxc\ also packs a bash dependence to be able to connect.
Ansible \cite{ansible} has a similar constraint at it requires Python on the nodes to execute their provisioning.

## The \texttt{vm-ramdisk} Flavour {#sec:vm-ramdisk}

\todo{one sentence to summarizes}
Creates QEMU virtual machines running in RAM.

### Construction

\nxc\ uses \nix\ to evaluate the configuration of every role in the composition.
\nix\ then generates the \texttt{initrd} of the profiles.
\todo{bof.}

### Deployment

\nxc\ uses QEMU \cite{qemu} to run the virtual machines.
The virtualization application requires the kernel and the \texttt{initrd} generated in the previous phase.
\nxc\ also sets up a Virtual Distributed Ethernet (or VDE) to manage the virtual network of the virtual machines.
It allows one virtual machines to connect to another using ssh for example.
This aims at reproducing the behaviour of a real distributed platform.


## The \texttt{g5k-ramdisk} Flavour {#sec:g5k-ramdisk}

\todo{one sentence to summarizes}
Creates an \texttt{initrd} for a quick deployment in memory without the need to reboot the host machine on \grid.

### Construction

As for the \texttt{vm-ramdisk} flavour presented in Section \ref{sec:vm-ramdisk}, \nxc\ also produces the kernel and \texttt{initrd} from the configuration of each role in the composition.
This generation relies on \nix\ and \nixos.
\todo{bof.}

### Deployment

For this flavour, \nxc\ leans on the \texttt{kexec} Linux system call.
\texttt{kexec} allows to boot in a new kernel from the current running one.
It skips the initialization of the hardware usually done by the BIOS thus avoiding an entire reboot of the machine (also called *soft reboot*).
It hence reduces the time to boot the new kernel.
The \texttt{kexec} commands takes as input the desired kernel, the \texttt{initrd} and parameters for the kernel.
\nxc\ thus passes the kernel and \texttt{initrd} generated in the construction phase to \texttt{kexec}.
As \nxc\ produces a single image containing the profiles of all the roles, it needs to indicate to each node of its role during the deployment.
To achieve this, we pass this information using the kernel parameters and setting up environment variable based on the role.
\nxc\ also uses these kernel parameters to pass ssh keys and information about the other hosts.

\begin{figure}
    \centering
    \includegraphics[width = 0.48\textwidth]{./figs/http_server.pdf}
    \caption{
Mechanism for the nodes to get the deployment information.
For a small number of nodes, the information are passed via the kernel parameters.
For a higher number of nodes, this is not possible due to the size limit of the kernel parameters (4096 bytes).
\nxc\ thus starts a HTTP server on the frontend and passes its IP address to the nodes via the kernel parameters.
The nodes then query this server to retrieve the deployment information.}\label{fig:http_server}
\end{figure}

However when the users asks to deploy too many nodes, we cannot send all the deployment information using the kernel parameters as the size of these information increases with the number of nodes and surpass the limit size of 4096 bytes.
To deal with this, \nxc\ starts a light HTTP server on the cluster frontend for the duration of the deployment.
We pass the address of this server using the kernel parameters.
Then, the nodes query this server with \texttt{curl} and get the information linked to their roles.
Note that the deployed images do not include a HTTP server but only the \texttt{curl} application to get the data.
Figure \ref{fig:http_server} represents how the nodes get the deployment information based on the quantity of nodes involved.

## The \texttt{g5k-image} Flavour

\todo{one sentence to summarizes}
Creates a tarball of the image for the deployment on \grid.

### Construction

As for the previous flavours, \nxc\ generates the kernel, \texttt{initrd}.
In this case however, we also need to produce a tarball that can then be deployed onto the machines.
The main point is to also pack the content of the \store.
This requires to put the necessary packages and their runtime dependencies in the image, but also to configure the \nix\ database to ensure the consistency between the latter and the \store.
The image is then compressed into a tarball.
\nxc\ uses compression parameters to prioritize compression speed and thus potentially degrading the final image size \cite{pixz}.
A larger tarball means longer deployment times.
There thus exists a tradeoff between speed and size that we did not explore at the moment.

### Deployment

To deploy the image constructed in the previous phase, \nxc\ lies on the \kad\ \cite{georgiou2006tool} application.
The \kad\ tool deploys environments onto machines.
It uses a simple description file in YAML that easily can be generated.
\nxc\ thus creates the \kad\ description file indicating the path of the compressed image, kernel and \texttt{initrd} paths along information such as the filesystem type.
Once the description file generated, \nxc\ calls the \kad\ application.

As for the \texttt{g5k-ramdisk} flavours in Section \ref{sec:g5k-ramdisk} the information of deployment are passed through the kernel parameters.
In the case of many nodes to deploy, these information will be larger than the maximum kernel parameters size.
Hence, \nxc\ will use, as for \texttt{g5k-ramdisk} deployment, a light HTTP server to distribute the information to every node (see Figure \ref{fig:http_server}).
