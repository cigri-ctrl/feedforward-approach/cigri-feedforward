# Towards a better usage of the resources {#sec:better_usage}

<!-- comment
In this section we present a PI controller for regulating the injection of BE jobs to achieve
close to 100% usage of the cluster

We then present an adaptive P controller requiring less knowledge about the jobs to run


we should also present some of the jobs and plots of the compas paper
-->

INTRO

We first suppose that there are no premium user jobs.

In this section, we present a solution based on \CT\ to regulate the injection of \bag\ tasks from \cigri\ to improve the usage of a computing cluster.

## Problem Definition

In this work, we want to improve the usage of a set of computing clusters.

One challenge is to keep enough \bag\ jobs in the waiting queue of \oar\ so they can be started immediately when some resources are freed, but not too many to not overload the waiting queue.
An overload of the waiting queue can lead to a longer response time for the controller, as \oar\ will schedule the jobs no matter if they are destined to be killed or not.
A long response time can degrade the Quality-of-Service for the users.



We make the following hypothesis:

#### There is only one cluster in the grid

This hypothesis allows us to focus on a single cluster.
The generalization to several clusters can be done by considering one autonomic controller per cluster.
An interesting thing to consider would be the affinity between \cigri\ campaigns and cluster.
But we leave this as future work.


#### There is no premium jobs

We consider premium jobs as perturbation.
Thus, as a first step, we consider the system with no perturbation.
In Section \todo{insert sec ref here}, we will introduce premium jobs.


#### \cigri\ cannot kill one of its jobs

\todo{}


#### There are no other \be\ jobs in the system besides \cigri's

\todo{}

#### The total number of resources in the cluster is constant

We do not take into account the variation of the availability of the nodes.
Meaning that no node are removed or added to the set of available nodes.
Note that if we can have a dynamic sensor of the number of available resources in the cluster, the remaining of the paper holds.

## Control Formulation

\begin{table}
\centering
\caption{Summary of the notations used.}\label{tab:notations}
\begin{tabular}[t]{l p{7cm}}
\toprule
Notation & Definition\\
\midrule


$u_k$ & Number of \bag\ resources submitted by \cigri\ at iteration $k$\\

$w_k$ & Number of \be\ resources in the \oar\ wainting queue at iteration $k$\\

$r_k$ & Number of used resources in the cluster at iteration $k$\\

$y_k$ & Output of the sensor at iteration $k$. ($y_k = r_k + w_k$)\\

$r_{\max}$ & Total number of resources in the cluster (supposed constant)\\

$y_{ref}$ & \emph{Reference value}, or desired state to maintain. ($y_{ref} = (1 \pm \varepsilon)\times r_{\max}$ with $\varepsilon \geq 0$)\\

$\Delta t$ & Time between two \cigri\ submissions (constant, chosen by the system administrator)\\

$e_k$ & Control error. The distance between the desired state and the current state ($e_k = y_{ref} - y_k$)\\

$K_p, K_i$ & Proportional and Integral gains of a PI Controller\\


\bottomrule
\end{tabular}
\end{table}

Our knob of action is the number of \bag\ jobs submitted by \cigri\ at each of its iterations.
To sense the current state of the clusters, we can query the API of the \oar\ schedulers to get the number of resources currently used, the number of \be\ jobs waiting, or the provisional schedule of the jobs.

We will actually consider resources and not jobs.
This consideration remove some degenerate situations.
One example would be \bag\ jobs using 2 resources each and having one idle resource on the cluster.
By considering jobs instead of resources, the controller could continue the injection of \bag\ jobs thinking that there is always some idle resources, even though it cannot be used.


We note:

- $u_k$: the number of \bag\ resources sent by \cigri\ at iteration $k$.

- $r_k$: the number of used resources at iteration $k$.

- $w_k$: the number of resources from \be\ jobs in the \oar\ waiting queue at iteration $k$.

- $r_{\max}$: the total number of resources in the cluster. We suppose it constant.


We thus want to regulate the quantity "number of currently used resources + number of \be\ resources waiting" around the total number of resources available in the cluster.
Meaning that we want to regulate $r_k + w_k$ around $r_{\max}$ by varying the value of $u_k$.

We note $y_k = r_k + w_k$.
We also note the *reference value* $y_{ref} = r_{\max}$.
The reference value could be $(1 \pm \varepsilon)\times r_{\max}$, with $\varepsilon > 0$, based on the cluster administration preferences.
A reference value greater than $r_{\max}$ could lead to longer response time for the controller and more killing of \be\ jobs, whereas a reference value smaller than $r_{\max}$ will leave some resources idle.

## System Analysis

We are now looking for a relation between $u_k$ and $y_k$.


Let us take a campaign with a mean execution time of $\bar{t_j}$.
In steady state, the amount of jobs finishing at each iteration $k$ is $\frac{\Delta t}{\bar{t_j}} \times r_k$

\begin{figure*}
\centering
\begin{subfigure}{0.55\textwidth}
  \includegraphics[width = \textwidth]{./figs/identification.pdf}
  \caption{Identification experiment. We vary the size of the batch that \cigri\ send to \oar\ as steps. \cigri\ first submits batches of 5 jobs for 20 iterations, then batches of 10 jobs for another 20 iterations, then 15 jobs and finally 20 jobs.}
  \label{fig:identification}
\end{subfigure}
\hfill
\begin{subfigure}{0.4\textwidth}
  \includegraphics[width = \textwidth]{./figs/hist_exec_times.pdf}
  \caption{Histogram of the execution times for a \cigri\ campaign during the identification experiment. The campaign is composed of synthetic jobs (see Section \todo{}) of theoretical duration 60 seconds.}
  \label{fig:hist_exec_times}
\end{subfigure}
\caption{\todo{}}
\end{figure*}



Let us consider a campaign of jobs with 60 seconds execution time.
We modified \cigri\ to submit the jobs of the campaign as steps.
Meaning that for 20 \cigri\ iterations, jobs are submitted by batches of 5.
Then, for the next 20 iterations, by batches of 10, then 15 and finally 20.
We record the output of our sensor during the entire experiment.
Figure \ref{fig:identification} depicts the results of the identification.

We observe that around 1500 seconds, \cigri\ starts to overflow the waiting queue.
And as a result, the value of the sensor does not stabilize but keep increasing, until 2600 seconds where there are no more submission from \cigri.
This can be explained by the fact that \cigri\ is submitting more jobs than the cluster can process in one iteration.
Let $t_j$ be the execution time of the jobs of the campaign.
Note that we supposed that the jobs have the same execution times.
Then, the number of resources freed at each iteration is:
\begin{equation}\label{eq:rate_1}
  \frac{\Delta t}{t_j}\times r_{\max}
\end{equation} 
In our experiment, $\Delta t$ would be 30 seconds, $t_j$ 60 seconds, and $r_{\max}$ 32 resources.
Meaning that the cluster can free up to $16$ resources per iteration.

However, when we look at the actual execution times of the jobs, we remark that they are not constant.
Figure \ref{fig:hist_exec_times} shows the histogram of the execution times of the \cigri\ campaign used for the identification experiment.
As we can see, no job actually lasted the theoretical 60 seconds. 
To be more precise, we can replace $t_j$ in Equation \ref{eq:rate_1} by the mean of the execution times ($\bar{t_j}$) or the median.
This precision leads to a processing rate of about 14.8 jobs per iterations.
This corrected rate thus explains why the value of the sensor starts growing when \cigri\ starts to submit batches of 15 jobs.

We want to avoid the overflowing the waiting queue.
Indeed, if the queue is not empty, \oar\ will try to schedule the \be\ jobs on the machines no matter if they are going to be killed soon or not.
By regulating the number of jobs in the waiting queue, we reduce the undesired killing of \be\ jobs
But the objective is to have a 100\% usage of the cluster.


## Model \& Control Design

From the discussion in the previous section, we can link the input of the system to the output to the system as:

\begin{equation}\label{eq:model}
\begin{aligned}
  y_{k + 1} & = \left(1 - \frac{\Delta t}{\bar{t_j}}\right) \times y_k + u_k \\
            & = \left(1 - \frac{\Delta t}{\bar{t_j}}\right) \times \left(r_k + w_k\right) + u_k
\end{aligned}
\end{equation}

The model in Equation \ref{eq:model} is what \CT\ calls a *first order model*.
A first order model can be expressed like: $y_{k+1} = a \times y_k + b \times u_k$, with $a, b \in \mathbb{R}$.


\todo{show a small example of how the model fits the data}

Knowing the model of the open-loop system, we can design a controller to regulate the closed-loop system.


A *controller* is a function taking as input the distance between the current state of the system and its desired state (\ie\ $y_{ref} - y_k$), this distance is called the *control error*, and returns the action on the actuator (\ie\ $u_{k+1}$).
\todo{add emph lines in the previous sentence}.

There are several ways to implement this controller.
The most popular is call *PI Controller*, for Proportional-Integral Controller.
Meaning that the response on the actuator has two components.
The first one is proportional to the current control error ($e_k = y_{ref} - y_{k}$), and the second one is proportional to the integral of the previous errors ($\int_0^k e_k$, or in discrete time: $\sum_{i = 0}^{k}e_i$). 
A PI Controller can be summarized by Equation \ref{eq:PI}:

\begin{equation}\label{eq:PI}
\begin{aligned}
  u_{k+1} & = K_p \times e_k + K_i \times \sum_{i = 0}^k e_i \\
          & = K_p \times \left(y_{ref} - y_k\right) + K_i \times \sum_{i=0}^k \left(y_{ref} - y_{i}\right)
\end{aligned}
\end{equation}

where, $K_p$ and $K_i$ are respectively the gains for the proportional component and the integral component.

The choice of these gains greatly influences the behavior of the closed loop system.
Large values of the gains can lead to oscillations and overshooting of the desired reference.
Small values of the gains can lead to slow response times.
Theorems from \CT\ or toolboxes can yield value of the gains to fit the desired closed-loop behavior.
This behavior is described by two characteristics:

- $k_s$: the time to reach the desired value.

- $M_p$: the allowed overshoot of the desired value.

Figure \ref{fig:closed_loop} shows various closed-loop behaviors for various couple $(k_s, M_p)$.


\begin{figure}
  \centering
  \includegraphics[width = 0.45\textwidth]{./figs/closed_loop_behavior.pdf}
  \caption{
    Closed-loop behavior of the system for different choices of parameters.
    \todo{Small values of $k_s$ lead to faster response, but also potential overshoot and oscillations.
    Large values of $M_p$ lead to ...}
  }
  \label{fig:closed_loop}
\end{figure}

\todo{cite hellerstein for the choice of kp, ki}.


<!-- comment
In [77]: a = 1 - 30/60

In [78]: b = 1

In [79]: ks = 2

In [80]: r = exp(-4/ks)

In [81]: theta = 0

In [82]: kp = (a - r * r) /b

In [83]: ki = (1 - 2 * r * cos(theta) + r * r)/b

In [84]: (kp, ki)
Out[84]: (0.48168436111126584, 0.7476450724155088)
-->

In our case, we took $k_s = 2$ and $M_p = 0$.
Thus, for jobs of $t_j = 60s$, this choice leads to values of gains to be: $K_p = 0.4817$ and $K_i = 0.7476$

## Evaluation

There are 3 settings to evaluate:

- when $r_{ref} < r_{\max}$. In this case, the model proposed in the previous section fully holds and the controller should maintain the reference easily.

- when $r_{ref} = r_{\max}$. In this case, we are at the limit of the validity of the model, but also in the range of action that we operate in.

- when $r_{ref} > r_{\max}$. In this case, the model probably does not hold anymore and the performance of the controller are degraded.


### $r_{ref} < r_{\max}$

### $r_{ref} = r_{\max}$

### $r_{ref} > r_{\max}$


