# Conclusion and Future Work {#sec:conclu}

This article has presented \nxc, a tool that enables the generation of reproducible distributed environments.
We have showed that \nxc\ deploys the exact same software stack on various platforms of different natures, without requiring specific work from users.
The software stack is reconstructible by design, as \nxc\ inherits its \repro\ properties from \nix\ and \nixos.
Our experiments showed that \nxc's \repro and platform versatility properties are achieved without deployment performance overhead in comparison to the existing solutions \kam\ and \enos.

