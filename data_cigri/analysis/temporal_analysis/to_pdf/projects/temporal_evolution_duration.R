library(tidyverse)


args = commandArgs(trailingOnly=TRUE)

filename = args[1]
outname = args[2]

# ldf <- read_csv("./temporal_data.csv", col_names = T)
df <- read_csv(filename, col_names = T)

df %>%
    ggplot(aes(x = period, y = mean_duration, color = project, group = project)) +
    geom_point(size = 2) +
    geom_errorbar(aes(ymin = mean_duration - bar, ymax = mean_duration + bar), width = 0.2) +
    facet_grid(project~., scales = "free_y") +
    xlab("") + ylab("Duration") +
    scale_y_log10(limits = c(1, NA), breaks=c(1, 10, 60, 10*60, 60*60, 10*60*60),labels=c("1s","10s","1m", "10m", "1h", "10h")) +
    theme_bw() +
    theme(legend.position = "bottom")

# ggsave("./figs/temporal_evolution_duration.pdf", width = 12, height = 12)
ggsave(outname, width = 12, height = 12)
