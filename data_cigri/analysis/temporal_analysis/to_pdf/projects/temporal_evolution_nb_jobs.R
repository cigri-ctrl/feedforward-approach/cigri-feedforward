library(tidyverse)

args = commandArgs(trailingOnly=TRUE)

filename = args[1]
outname = args[2]

# ldf <- read_csv("./temporal_data.csv", col_names = T)
df <- read_csv(filename, col_names = T)

df %>%
    ggplot(aes(x = period, y = n, color = project, group = project)) +
    geom_point(size = 2) +
    facet_grid(project~., scales = "free_y") +
    xlab("") + ylab("Total nb of jobs") +
    scale_y_log10() +
    ylim(0, NA) +
    theme_bw() +
    theme(legend.position = "bottom")

ggsave(outname, width = 12, height = 12)
