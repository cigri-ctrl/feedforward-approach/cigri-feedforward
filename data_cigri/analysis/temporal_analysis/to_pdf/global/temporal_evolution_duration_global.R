library(tidyverse)
library(reshape2)
library(patchwork)

args = commandArgs(trailingOnly=TRUE)

filename = args[1]
outname = args[2]

# df <- read_csv("./temporal_data_global.csv", col_names = T)
df <- read_csv(filename, col_names = T)

plot_duration <- df %>%
    select(period, mean_duration, med_duration) %>%
    melt(id.vars = c("period")) %>%
    ggplot(aes(x = period, y = value, linetype = variable, group = variable)) +
    geom_point(size = 2) +
    geom_line() +
    # geom_errorbar(aes(ymin = mean_duration - bar, ymax = mean_duration + bar), width = 0.2) +
    ggtitle("Evolution de la durée des tâches CiGri par Trimestre") +
    # xlab("Année-Trimestre") +
    xlab(NULL) +
    ylab("Durée des tâches") +
    scale_y_log10(limits = c(1, NA), breaks=c(1, 10, 60, 10*60, 60*60, 10*60*60),labels=c("1s","10s","1m", "10m", "1h", "10h")) +
    scale_linetype_discrete(name = "", breaks = c("mean_duration", "med_duration"), labels = c("Moyenne", "Médiane")) +
    theme_bw() +
    theme(
        legend.position = c(0.33, 0.25),
        legend.title = element_blank(),
        legend.box = "horizontal",
        legend.background = element_rect(fill = "white", color = "black"),
        axis.text.x = element_text(angle = 45, hjust=1)
    )

    # ggplot(aes(x = period, y = mean_duration, group = 1)) +
    # geom_point(size = 2) +
    # geom_line() +
    # geom_errorbar(aes(ymin = mean_duration - bar, ymax = mean_duration + bar), width = 0.2) +
    # ggtitle("Evolution de la durée moyenne des tâches CiGri par mois") +
    # xlab("Année-Mois") + ylab("Durée moyenne des tâches") +
    # scale_y_log10(limits = c(1, NA), breaks=c(1, 10, 60, 10*60, 60*60, 10*60*60),labels=c("1s","10s","1m", "10m", "1h", "10h")) +
    # theme_bw() +
    # theme(
    #     legend.position = "bottom",
    #     axis.text.x = element_text(angle = 45, hjust=1)
    # )
# ggsave("./figs/temporal_evolution_duration_global.pdf", plot = plot_duration, width = 6, height = 2.5)
ggsave(outname, plot = plot_duration, width = 6, height = 2.5)

# plot_nb_jobs <- df %>%
#     ggplot(aes(x = period, y = n, group = 1)) +
#     geom_point(size = 2) +
#     geom_line() +
#     ggtitle("Evolution du nb total de tâches CiGri executées par Trimestre") +
#     xlab("Année-Trimestre") + ylab("Nombre de tâches (log)") +
#     scale_y_log10(limits = c(1, 5000000)) +
#     theme_bw() +
#     theme(
#         legend.position = "bottom",
#         axis.text.x = element_text(angle = 45, hjust=1)
#     )
# 
# 
# p <- plot_duration / plot_nb_jobs
# 
# ggsave("./figs/temporal_evolution_total.pdf", plot = p, width = 6, height = 5)
# 
