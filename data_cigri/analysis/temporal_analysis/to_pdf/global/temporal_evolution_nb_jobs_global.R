library(tidyverse)

args = commandArgs(trailingOnly=TRUE)

filename = args[1]
outname = args[2]

# df <- read_csv("./temporal_data_global.csv", col_names = T)
df <- read_csv(filename, col_names = T)

df %>%
    ggplot(aes(x = period, y = n, group = 1)) +
    geom_point(size = 2) +
    geom_line() +
    ggtitle("Evolution du nombre total de tâches CiGri executées par Trimestre") +
    xlab("Année-Trimestre") + ylab("Nombre de tâches (log)") +
    scale_y_log10(limits = c(1, 5000000)) +
    theme_bw() +
    theme(
        legend.position = "bottom",
        axis.text.x = element_text(angle = 45, hjust=1)
    )

# ggsave("./figs/temporal_evolution_nb_jobs_global.pdf", width = 6, height = 2.5)
ggsave(outname, width = 6, height = 2.5)
